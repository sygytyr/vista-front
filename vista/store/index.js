import Vue from 'vue';

const languageList = ['en', 'uk', 'ru']

export const state = () => ({
  lang: 'uk',
  data: {},
  page: {},
  meta: {},
  allPage: {},
  loadedData: false,
  loadedPage: false,
  firstLoading: true
})

export const getters = {
  getLang (state) {
    return state.lang
  },
  getLanguageCode (state) {
    return state.lang === 'uk' ? 'ua' : state.lang;
  },
  getGlobalData(state){
    return state && state.data
  },
  getScripts (state) {
    return state && state.data && state.data.scripts
  },
  getFooter (state) {
    return state && state.data && state.data.footer
  },
  getSocials (state) {
    return state && state.data && state.data.socials
  },
  getMenu (state) {
    return state && state.data && state.data.menu
  },
  getLogo(state){
    return state && state.data && state.data.logo
  },
  getPage(state){
    return state && state.page
  },
  getNamedPage(state){
    return (name) => state && state.page[name]
  },
  getAllPage(state){
    return state && state.allPage
  },
  getMeta(state){
    return state && state.meta
  },
  getLoading(state){
    return state && state.loading
  },
  firstLoading(state){
    return state && state.firstLoading
  }
}

export const mutations = {
  setLang (state, lang = 'uk') {
    state.lang = lang
  },
  setLoading (state, value) {
      state.loading = value
  },
  setFirst (state) {
      state.firstLoading = false
  },
  SET_DATA (state, data) {
    state.data = {};
    state.loadedData = true;
    Vue.set(state, 'data', data)
  },
  SET_PAGE_DATA (state, data) {
    this.loadedPage = true;
    let pageName = data.id !== 'category-one' ?
      data.id : 'recept-category-one'
    Vue.set(state.page, pageName , data)
  },
  SET_ALL_PAGE_DATA (state, data) {
    state.allPage = {};
    Vue.set(state, 'allPage', data)
  },
  SET_META_DATA (state, data) {
    state.meta = {};
    Vue.set(state, 'meta', data)
  },
}

export  const actions = {
  async nuxtServerInit ({ commit, dispatch }, { params, redirect, route, app }) {
    const locale = app.i18n.locale === 'ua'? 'uk' : app.i18n.locale
    const { name: routeName } = route
    commit('setLang', locale);
    await dispatch('fetchData', { lang: locale });
    //if(!isInLanguageList) redirect({name: routeName, params: params})
  },
  async fetchData ({ commit, getters, state }, ) {
    let finalLang = this.$i18n.locale
    this.$axios.setHeader("Accept-Language", finalLang)
    let URLFinal = process.env.API_URL;


    try {
      const { data } = await this.$axios.get(URLFinal);
      commit('SET_DATA', data.attributes);
      return data
    } catch (error) {
      console.error('fetchGlobalData: ', error)
    }
  },
  async fetchPageData ({ commit, getters, state }, { url }) {
    commit('setLoading', true);

    let finalLang = this.$i18n.locale
    this.$axios.setHeader("Accept-Language", finalLang)
    let URLFinal = process.env.API_URL +'pages'+ url;

    try {
      let res = await this.$axios.get(URLFinal);
      let finalData = res.data
      const { blocks } = res.data

      for (const { attributes } of blocks) {
        if (attributes && attributes.requestUrl) {
          let par = url.split('?')[1]
          this.$axios.setHeader("Accept-Language", finalLang)
          try{
            const res = await this.$axios.get(attributes.requestUrl, {
              params: par
            })

            attributes.items = res.data;
          } catch (e){
            console.log(e);
          }
        }
      }

      // res = await res.json()
      commit('SET_PAGE_DATA', finalData);
      commit('SET_ALL_PAGE_DATA', finalData);
      commit('SET_META_DATA', res.data.meta);
      commit('setFirst')

      return res
    } catch (error) {
      console.error('fetchGlobalData: ', error)
    } finally {
      commit('setLoading', false);
    }
  },
}

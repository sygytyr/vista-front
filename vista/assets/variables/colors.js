module.exports = {
  'c-white': '#fff',
  'c-black': '#0b0a1d',
  'c-blue': '#006FF5',
  'c-blue-2': '#0053B6',
  'c-blue-dark': '#012057',
  'c-orange': '#F57723',
  'c-orange-2': '#E95D00',
  'c-red': '#E53700',
  'c-grey-light': '#EAEFF3',
  'c-grey-ligher': '#F6F8F9'
}

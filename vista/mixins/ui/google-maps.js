export default {
  watch: {
    loading (v) {
      this.$emit('onLoading', v)
    }
  },

  data () {
    return {
      loading: false,
      center: {
        lat: 50.105800,
        lng: 30.465260
      }
    }
  },

  methods: {
    loadedGoogleMapsAPI () {
      return new Promise(resolve => {
        const KEY = 'AIzaSyDnEgkY0Du_EaNjQsVVE8SMNGQA8U2dqEs'
        window['GoogleMapsInit'] = resolve

        const GMap = document.createElement('script')
        GMap.id = 'google-map-script'
        GMap.src = `https://maps.googleapis.com/maps/api/js?key=${KEY}&callback=GoogleMapsInit&region=IN`

        document.body.appendChild(GMap)
      })
    },

    async initMap () {
      this.loading = true
      if (document.querySelector('#google-map-script') === null) {
        await this.loadedGoogleMapsAPI()
      }

      if (!navigator.geolocation) {
        console.error('Geolocation error')
      } else {
        await navigator.geolocation.getCurrentPosition(this.successGeolocation, () => {
          console.error('Geolocation error')
        })
      }

      this.loading = false
    },

    successGeolocation (position) {
      /* eslint-disable */
      const latitude = position.coords.latitude
      const longitude = position.coords.longitude

      this.center = { lat: latitude, lng: longitude }
    }
  }
}

import { mapState, mapGetters } from 'vuex'
import isEmpty from 'lodash/isEmpty'
import appendScripts from '~/assets/js/helpers/append-scripts'

let scriptsAppened = false

export default {
  computed: {
    ...mapGetters(['getAllPage', 'getScripts']),
    seo() {
      return this && this.getAllPage && this.getAllPage.meta && this.getAllPage.meta.seo
    }
  },

  head () {
    if(!this.seo) {
      return {}
    }

    return {
      title: this.seo && this.seo.title || '',
      meta: [
        { name: 'robots', content: this.seo && this.seo.robots|| '' },
        { name: 'keywords', content: this.seo &&  this.seo.keywords || '' },
        { name: 'description', content: this.seo && this.seo.description || '' },
        { name: 'og:title', content: this.seo && this.seo['og:title'] || '' },
        { name: 'og:url', content: this.seo && this.seo['og:url'] || '' },
        { name: 'og:site_name', content: this.seo && this.seo['og:site_name'] || '' },
        { name: 'og:description', content: this.seo && this.seo['og:description'] || '' },
        { property: 'og:image', content: this.seo && this.seo['og:image'] || '' }
      ],
      link: [...this.generateLinks()],
      script: [
        {
          json: this.generateBreadcrumbsMicromark(this.seo.breadcrumbs),
          type: 'application/ld+json'
        }
      ]
    }
  },

  mounted () {
    if (!scriptsAppened) {
      appendScripts(this.getScripts)
      scriptsAppened = true
    }
  },

  methods: {
    generateLinks () {
      const result = []

      // canonical
      if (!isEmpty(this.$route.query) && !this.$route.query.page) {
        result.push({ rel: 'canonical', href: `${this.origin}${this.$route.path}` })
      }

      return result
    },

    generateBreadcrumbsMicromark (breadcrumbs) {
      if (!breadcrumbs) return {}

      return {
        '@context': 'http://schema.org',
        '@type': 'BreadcrumbList',
        'itemListElement': breadcrumbs.map((breadcrumb, index) => ({
          '@type': 'ListItem',
          'position': index + 1,
          'item': {
            '@id': `${this.origin}${breadcrumb.url || ''}`,
            'name': breadcrumb.label
          }
        }))
      }
    }
  }
}

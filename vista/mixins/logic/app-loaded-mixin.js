import createEvent from '~/assets/js/utils/create-event'

// global event. trigger for APP fully ready
const appLoadedEvent = createEvent('app:loaded')

export default {
  mounted () {
    // remove preloader while development
    // if (process.env.NODE_ENV === 'development' && process.env.PRELOADER_ON_DEV === 'false') {
    //   this.$store.commit('ui/SET_OVERLAY_VISIBILITY', false)
    //   document.getElementById('__nuxt').style.overflow = ''
    // }
    document.getElementById('__nuxt').style.overflow = ''
    this.$store.commit('ui/SET_OVERLAY_VISIBILITY', false)
  },

  methods: {
    hidePreloader () {
      this.$store.commit('ui/SET_OVERLAY_VISIBILITY', false)
      document.documentElement.removeEventListener('preloader:done', this.hidePreloader)
    },
    onAppLoaded () {
      document.documentElement.dispatchEvent(appLoadedEvent)
      // if (!isPreloaderDone) {
      //   document.documentElement.addEventListener('preloader:done', this.hidePreloader)
      // }

      // const PRELOADER_MIN_VISIBILITY_MS = 200
      // setTimeout(() => {
      //   document.documentElement.dispatchEvent(appLoadedEvent)
      // }, PRELOADER_MIN_VISIBILITY_MS)
    }
  }
}

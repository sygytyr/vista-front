import Vue from 'vue'

Vue.use({
  install (Vue) {
    Vue.mixin({
      methods: {
        generateImageUrl (path) {
          if (!path) return '';
          return process.env.BASE_URL + path
        }
      }
    })
  }
});

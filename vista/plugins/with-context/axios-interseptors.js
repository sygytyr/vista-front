
const STATUS_CODES_ERROR = [404, 500]

export default function ({ $axios, store, app }) {
  $axios.onRequest(async (config) => {
    return config
  })
  $axios.onError(({ response, request }) => {
    console.error('Error caught in axios-interseptors.', response, request)

    if (process.env.FAKE_API_ENABLED !== 'true') {
      showErrorPage(response.status, 'real API error')
      return response
    }

    if (process.env.FAKE_API_ENABLED === 'true' && response.headers['fake-api'] === 'true') {
      showErrorPage(response.status, 'fake API error')
    }

    function showErrorPage (statusCode, message) {
      if (STATUS_CODES_ERROR.includes(statusCode)) {
        app.context.error({
          statusCode,
          message: `(${statusCode}) ${message}`
        })
      }
    }
  })
}


const build = require('./nuxt-config/_build')
const env = require('./nuxt-config/_env')
const head = require('./nuxt-config/_head')
const modules = require('./nuxt-config/_modules')
const options = require('./nuxt-config/_options')
const plugins = require('./nuxt-config/_plugins')
const render = require('./nuxt-config/_render')
const i18n = require('./nuxt-config/_i18n')

module.exports = {
  mode: 'universal',
  build,
  styleResources: {
    scss: ['~assets/style/variables/index.scss',
      '~assets/style/mixins/index.scss']
  },
  css: [
    '~assets/style/index.scss'
  ],
  env,
  head,
  modules,
  plugins,
  render,
  i18n,
  ...options,
}

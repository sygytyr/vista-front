const defaultEnvironment = 'production';
process.env.CONFIG = process.env.CONFIG || process.env.npm_config_env || defaultEnvironment;
// used require instead of import as import() is async
let config;

try {
  config = require(`./env-list/${process.env.CONFIG}`).config
} catch (e) {
  config = require(`./env-list/${defaultEnvironment}`).config
}

export function setEnvirovment () {
  for (const prop in config) {
    process.env[prop] = config[prop]
  }
}

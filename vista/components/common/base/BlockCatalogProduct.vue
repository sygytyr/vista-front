<template>
  <section class="catalog">
    <BaseTitle :title="title" class="h2" level="2"/>
    <CatalogHeader
      :tabs="tabs"
      :searchLabel="translation[getLang]['search']"
      :active="activeTab"
      :change-active="changeActive"
      @search="search"
    />
    <div class="row catalog__content" v-if="activeItems && activeItems.length">
      <div
        v-for="item in activeItems"
        :key="item.link.slug"
        class="col col-md-3 col-lg-4"
      >
        <ProductBox
          :image-desktop="item.imageDesktop && item.imageDesktop.src? item.imageDesktop: item.imageMobile"
          :imageMobile="item.imageMobile && item.imageMobile.src? item.imageMobile: item.imageDesktop"
          :label="item.label"
          :description="item.description"
          :form="item.form"
          :link="item.link"
        />
      </div>

    </div>
    <div class="row catalog__content" v-else>
      {{translation[getLang]['not-found']}}
    </div>
    <div class="d-flex j-center"><loading-button v-if="linkToLoad"
                                                 :loading="loading"
                                                 :button-text="btnText"
                                                 type3
                                                 @click.native="loadMore"
    /></div>

  </section>
</template>

<script>
  import ProductBox from '@/components/product/ProductBox'
  import axios from 'axios'
  import CatalogHeader from '@/components/common/base/CatalogHeader'
  import BaseTitle from "@/components/common/base/BaseTitle";
  import BaseButton from "@/components/common/base/BaseButton";
  import translation from 'static/lang';
  import {mapActions, mapGetters} from "vuex";
  import LoadingButton from "~/components/common/stripped-container/LoadingButton";
  import Vue from "vue";

  export default {
    name: 'BlockCatalogProduct',
    components: {
        CatalogHeader,
        ProductBox,
        BaseButton,
        BaseTitle,
      LoadingButton
    },
    props: [
      'button', 'dynamicItems',
      'requestUrl', 'searchLabel',
      'tabs', 'title', 'searchUrl',
      'withTabs'
    ],
    data () {
      return {
        items: this.dynamicItems,
        next: null,
        loading: false,
        translation,
        linkToLoad: '',
        activeTab: this.$route.query.isReceiptTab || this.requestUrl.match('is_bad=1') ? 1 : 0,
      }
    },
    computed:{
      ...mapGetters(['getLang','getNamedPage' , 'getLoading']),
      getPage(){
        return this.getNamedPage && this.getNamedPage('recept-category-one')
      },
      content() {
        return this.getPage && this.getPage.blocks
      },
      products() {
        return this.content && this.content[1] && this.content[1].attributes
      },
      activeItems(){
        return this.items
      },
      btnText() {
        return this.products && this.products.button && this.products.button.label;
      },
      itemsObj(){
        return this.products && this.products.items
      },
      links(){
        return this.itemsObj && this.itemsObj._links
      },
      nextLink() {
        return this.links && this.links.next && this.links.next.href
      },
    },

    watch: {
      dynamicItems: function (){
        this.items = this.dynamicItems
      },
      nextLink: {
        immediate: true,
        handler: function (){
          this.linkToLoad = this.nextLink
        }
      }
    },
    mounted() {
      if (!this.checkForProfessionalSkill() && this.$route.query.isReceiptTab !== false && this.withTabs) {
        return this.$router.push(this.localeRoute({ name: 'products-notification', params: { previousRoute: this.$route.path }}))
      }
    },
    methods: {
      ...mapActions(['fetchData', 'fetchPageData']),
      async search(value){
        try {
          let res = await axios( process.env.API_URL + this.searchUrl+'?label=' + value);
          this.items = res && res.data && res.data.items.map(el => el || [])
        } catch (e){
          console.log(e)
        }
      },
      checkForProfessionalSkill() {
       return JSON.parse(localStorage.getItem('isProfessionalSkill'))
      },
      async changeActive (index) {
        this.activeTab = index;
        if (index === 1 && !this.checkForProfessionalSkill()) {
          return this.$router.push(this.localeRoute({ name: 'products-notification', params: { previousRoute: this.$route.path }}))
        }
        await this.fetchPageData({url: this.tabs[index].url.replace(/\/..\//, '/'),  lang: this.getLang});
      },
      async loadMore () {
        this.loading = true
        try {
          const res = await axios({
            method: 'GET',
            url: this.linkToLoad,
            params: {
              currentPage: this.currentPage
            }
          })
          const nextLink = res.data._links && res.data._links.next && res.data._links.next.href || ''
          this.linkToLoad = nextLink;
          this.items = this.items.concat(_get(res.data, 'items', []))
        } catch (error) {
          console.error(error)
        } finally {
          this.loading = false
        }
      }
    }
  }
</script>

<style lang="scss" scoped>
  .catalog {
    margin-bottom: 60px;
    .h2 {
      margin-bottom: 36px;

      @include respOnly(sm) {
        margin-bottom: 24px;
      }
    }

    &__content {
      margin-bottom: 10px;
      display: grid;
      grid-gap: 30px;
      grid-template-columns: 1fr;
      font-family: $font-main-regular;

      @include respond-from('special'){
        grid-template-columns: 1fr 1fr;
      }

      @include respond-from('large'){
        grid-template-columns: 1fr 1fr 1fr;
      }
    }

    &__svg {
      display: block;
      width: 40px;
      height: 40px;
      margin: 100px auto 100px;
    }

    &__btn {
      min-width: 100%;
      margin-bottom: 60px;
    }
  }
</style>

// import Vue from 'vue'

export default {
  data: () => ({
    isSending: false,
    isSent: false,

    errors: {}
  }),

  watch: {
    $router: 'clearErrors'
  },

  beforeDestroy () {
    this.resetForm()
  },

  methods: {
    /**
     *
     * @param {string} name - input name
     * @return {string} - return error text
     */
    hasError (name) {
      return _get(this.errors, `${name}`, '')
    },

    /**
     * Function that called on 422 error catch
     * @param {object} errors - list of errors received from response
     */
    addErrors (errors) {
      this.errors = errors
    },

    /**
     * remove errors for specific name
     */
    removeError (name) {
      delete this.errors[name]
    },

    /**
     * clear all errors
     */
    clearErrors () {
      this.errors = {}
    },

    /**
     * reset form to default state
     */
    resetForm () {
      this.isSent = false
      this.isSending = false

      // clear every param of formData.
      Object.keys(this.formData).forEach(param => {
        if (Array.isArray(this.formData[param])) {
          this.formData[param] = []
        } else if (this.formData[param] instanceof Object) {
          this.formData[param] = {}
        } else {
          this.formData[param] = ''
        }
      })

      if (this.$refs.form) this.$refs.form.reset()

      // clear form errors
      this.clearErrors()
    },

    sending () {
      this.isSent = false
      this.isSending = true

      this.$emit('sending')
    }
  }
}

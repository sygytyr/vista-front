import FormError from './FormError'

export default {
  inheritAttrs: false,
  components: { FormError },
  props: {
    tag: String,
    label: String,
    value: '',
    error: String,
    items: Array,
    theme: {
      type: String,
      default: 'default'
    }
  },

  data: () => ({
    isOnFocus: false,
    marginBottom: 0
  }),

  mounted () {
    // set initial margin bottom for animation
    this.marginBottom = parseInt(getComputedStyle(this.$el).marginBottom)
  }

}

/**
 * Additional build configurations & webpack config extension
 * https://nuxtjs.org/api/configuration-build
 */

const path = require('path')
const webpack = require('webpack')
module.exports = {
  analyze: false,
  extractCSS: true,
    // fix devtools styles break when onchange
  // cssSourceMap: false,
  plugins: [
    // globals
    new webpack.ProvidePlugin({
      gsap: ['gsap', 'gsap'],
      _get: ['lodash/get'],
      $propTypes: [path.resolve(__dirname, '../assets/js/modules/PropTypes/index.js'), 'default']
    })
  ],

  extend (config, { isDev, isClient }) {
    // eslint
    // if (isDev && isClient) {
    //   config.module.rules.push({
    //     enforce: 'pre',
    //     test: /\.(js|vue)$/,
    //     loader: 'eslint-loader',
    //     exclude: /(node_modules)/
    //   })
    // }
    // config.module.rules.forEach(rule => {
    //   if (/scss/.test(rule.test.toString())) {
    //     rule.oneOf.forEach(key => {
    //       if (key.use) {
    //         key.use.push({
    //           loader: '@epegzz/sass-vars-loader',
    //           options: {
    //             syntax: 'sсss',
    //             files: [
    //               path.resolve(__dirname, '../assets/variables/index.js')
    //             ]
    //           }
    //         })
    //       }
    //     })
    //   }
    // })

  },

}

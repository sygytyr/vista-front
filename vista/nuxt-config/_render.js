
import shrinkRay from 'shrink-ray-current'
module.exports = {
  compressor: shrinkRay(),
  bundleRenderer: {
    shouldPreload (file, type) {
      return ['script', 'style', 'font'].includes(type)
    }
  }
}

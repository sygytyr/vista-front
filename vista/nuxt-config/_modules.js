module.exports = [
  [
    '@nuxtjs/axios', {
      credentials: false,
      proxyHeaders: false,
      debug: false
    }
  ],
  'nuxt-i18n',
  '@nuxtjs/style-resources',
  "@nuxtjs/svg",
  ['nuxt-gmaps', {
    key: process.env.GOOGLE_API,
  }],
  '@nuxtjs/dotenv'
]

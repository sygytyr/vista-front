module.exports = {
  locales: ['en', 'ru', 'uk'],
  defaultLocale: 'uk',
  // lazy: true,
  vueI18n: {
    fallbackLocale: 'uk'
  }
}

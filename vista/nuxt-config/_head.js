
module.exports = {
  meta: [
    { charset: 'utf-8' },
    {
      name: 'viewport',
      content: 'width=device-width, initial-scale=1, maximum-scale=1, viewport-fit=cover'
    },
    { property: 'og:type', content: 'website' }
  ],
  link: [
    {
      rel: 'icon',
      type: 'image/x-icon',
      href: '/icon.png',
    },
  ],
}

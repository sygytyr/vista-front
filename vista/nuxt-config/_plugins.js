
module.exports = [
  // only on client
  {
    src: '~/plugins/client',
    mode: 'client'
  },
  {
    src: '~/plugins/youtube',
    mode: 'client'
  },
  {
    src: '~/plugins/with-context/router-transition',
    mode: 'client'
  },

  // both on server and client
  {
    src: '~/plugins/universal'
  },
  {
    src: '~plugins/vue-js-modal.js'
  },
  {
    src: '~/plugins/with-context/axios-interseptors'
  },
  {
    src: '~/plugins/getMediaUrl'
  }
]

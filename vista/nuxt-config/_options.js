module.exports = {
  loadingIndicator: {
    name: 'circle',
    color: '#3B8070',
    background: 'white'
  },

  layoutTransition: {
    mode: 'out-in',
  },
  pageTransition: {
    mode: 'out-in',
    name: 'page'
  }
}
